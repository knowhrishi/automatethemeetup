# Automate The Meetup

A step towards automating the process of organizing monthly Python Pune meetups.

## Tools used for Organizing meetup

* GMAIL.COM for receiving RSVP emails
* meetup.com for announcement and showing rsvp

## How Python Pune meetup is Organized?

Python Pune meetup usergroup is totally volunteer driven initiative.
At the starting of each month, below is the list of activities are
performed by volunteers in sequence:

* Send a venue email to mailing list
  * Venue email contents:
    * Python Pune meetup introduction
    * Date and duration of the meetup
    * Prescribed format of the meetup
      * Workshop
      * Workshop + Talks
      * Talks
      * Devsprints

* Once Venue is confirmed, we need to announce the meetup on meetup.com
  in a prescibed format
  * Announcement email contents:
    * Meetup Title
    * A short description what is going to happen
    * RSVP method:
      * Using email system in a prescribed format
        * Email template needs to be following details
          * Name
          * Email
          * Valid meetup user ID
      * meetup.com rsvp method
      * If chargeable, How to make payment
    * Date, Duration and Venue of the meetup
    * Requirements of the meetup
    * Basic questionaries
    * Contact Info
    * Volunteers hosting the meetup
    * Meetup link
  * Email needs to be sent to different mailing list and meetup message board.

* From last one year, we are using email system for handling RSVP. Once meetup
  got announced, attendees start sending the email in prescribed format till
  date when meetup is going to happen.

* Volunteers needs to monitor the gmail meetup mail box and then grep the
  meetup ID and confirm their RSVP on meetup.com. Once RSVP is done,
  meetup.com will automatically notify the user. If meetup ID not found,
  the organizer again asks for the same with proper guidelines on how to
  search meetup ID using meetup.com.

* One day before the event, the organizer needs to generate the full attendees
  list with Only names and send the Venue host.

* And after all these above steps, the show runs.

## How to automate the above steps to avoid burden?

We are going to automate the whole steps in series of meetups.
In the coming Devsprint, our plan is to create scripts for individual tasks and
then merge them into a single projects.

Below is the list of scripts needs for following tasks:
* Template generator script for
  * Venue email
  * Announcement email
  * RSVP email

* Extract date from announcement email and create an event on meetup.com and return the
  event url.

* Send emails to prescribed lists based on Venue/Announcement email

* Once RSVP is open, we need a list of emails received on gmail inbox in a particular
  duration.

* Scrap Name, meetupID, email for each of the RSVP email

* Need a script to look whether a meetup user id exists or not.
  * If exists, then confirm the RSVP and send email stating RSVP confirmed
  * If not create an auto reply email for the same

* Get a final list of attendees coming for the meetup.

## Where to monitor all these datas?

Once we have all the scripts ready, we are going to create a one page webapp
to show the following data in tabular format and handle all the stuff from
there.

* Meetup Name
  * List of people RSVP status and action needs to be taken based on gmail data
  * Organizer to act [approve, Deny, On Hold, Reject]
  * Finally generate attendees list in different formats.

## Next course of action:

* Let's manage all the stuff through IRC bot
* Need server and access level for managing the app
* Configuration / Logging / Monitoring of the application


## Tools requirements for completing above project

* Python 3
* Third party libraries to interact with Gmail and meetup.com
